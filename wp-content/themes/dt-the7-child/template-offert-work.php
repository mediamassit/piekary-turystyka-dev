<?php 
/**
 * Template Name: Oferta zakładowa
 */
get_header();
?>
<div id="content" class="content" role="main">
	<div class="articles-list loading-effect-fade-in" data-cur-page="1">
		<?php 
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$args = array(
			'post_type'              => array( 'offer' ),
			'post_status'            => array( 'Publish' ),
			'paged'          => $paged,
			'tax_query' => array(
				array(
					array(
						'taxonomy' => 'offert_cat',
						'field'    => 'slug',
						'terms'    => 'Oferta zakładowa'
						),
					),
				),
			);
		$query = new WP_Query( $args );
		;?>
		<?php if($query-> have_posts() ):
			while ($query -> have_posts()) : $query->the_post(); ?>
				<article class="post news_post project-odd post-191 type-post status-publish format-standard has-post-thumbnail hentry category-bez-kategorii category-lifestyle-hobby category-marketing tag-business tag-corporate tag-news tag-post tag-tags tag-wordpress category-1 category-4 category-5 description-off" style="padding-top:30px;">
					<div class="blog-media wf-td-custom mobile-hidden" style="width: 30%;">
						<a href="<?php the_permalink() ;?>" class="rollover alignleft this-ready">
						<?php $thumb = get_the_post_thumbnail_url(get_the_id(),'offert-size');
							if($thumb):
						;?>
							<img class="lazy-load preload-me is-loaded" src="<?php echo $thumb ;?>" alt="" width="979" height="700">
							<?php else: ;?>
							<img class="lazy-load preload-me is-loaded" src="<?php echo get_stylesheet_directory_uri();?>/img/else.jpg" alt="" width="979" height="700">
							<?php endif;?>
						
						<i></i></a>
					</div>
					<div class="blog-content wf-td-custom" style="width: 70%;">
						<h3 class="entry-title title-offert"><a href="<?php the_permalink();?>" title="<?php the_title() ;?>" rel="bookmark"><?php the_title() ;?></a></h3>
							<?php $term = get_field('term'); 
								$term = lenght_cut($term);
								$place = get_field('from');
								$place = lenght_cut($place);
								$price = get_field('price');
							?>
						<div class="offer-before-info">
							<?php if($term): ;?><p class="offer-info"><strong>Termin:</strong></p><?php endif;?>
							<?php if($place): ;?><p class="offer-info"><strong>Wyjazd z:</strong></p><?php endif;?>
							<?php if($price): ;?><p class="offer-info"><strong>Cena:</strong></p><?php endif;?>
						</div>
							<p class="offer-info"><strong class="mobile_display">Termin: </strong><?php echo $term;?></p>
							<p class="offer-info"><strong class="mobile_display">Wyjazd z: </strong><?php echo $place;?></p>
							<p class="offer-info"><strong class="mobile_display">Cena: </strong><?php echo $price;?></p>
						<a href="<?php the_permalink();?>" class="home_post_more" rel="nofollow">Czytaj dalej</a>
					</div>
				</article>
		<?php endwhile; endif ;?>
		<?php dt_paginator( $query, array( 'class' => 'paginator' ) );?>
	</div>
</div>

<?php get_template_part( 'sidebar' ); ?>


<?php get_footer() ;?>