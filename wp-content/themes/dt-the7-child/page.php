<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package presscore
 * @since presscore 1.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

$config = Presscore_Config::get_instance();
$config->set('template', 'page');

get_header(); ?>
		<?php if(is_front_page()): ?><!---->
			<?php 
				$title = get_field('ofert_title',option);
				$subtitle = get_field('ofert_desc',option);
				$url = get_field('file_url', option);
				$travels = get_field('travel_box', option);
			;?>
			<div class="margin_wrap">
				<div class="travel_box">
					<div class="travel_box_text">
						<h2><?php echo $title;?></h2>
						<p><?php echo $subtitle;?></p>
						<?php if($url):;?>
							
						<a href="<?php echo $url['url'];?>" download><p class="travel_box_btm">Pobierz najnowszy katalog</p></a>
						<?php endif; ?>
					</div>
					<?php foreach($travels as $travel): ?>
					<div class="travel_box_link">
						<a href="<?php echo $travel['link'];?>"><p class="travel_box_title"><?php echo $travel['title'];?></p>
						<img src="<?php echo $travel['img']['url'];?>" alt=""></a>
					</div>
					<?php endforeach ;?>
<!--
					<div class="travel_box_link">
						<a href=""><p class="travel_box_title">Aktualna oferta wycieczki</p>
						<img src="<?php echo get_stylesheet_directory_uri();?>/img/basen.jpg" alt=""></a>
					</div>
					<div class="travel_box_link">
						<a href=""><p class="travel_box_title">oferta wycieczki</p>
						<img src="<?php echo get_stylesheet_directory_uri();?>/img/bus.jpg" alt=""></a>
					</div>
-->
				</div>
			</div>
		<?php endif ;?><!---->
			
		<?php if ( presscore_is_content_visible() ): ?>
			
			<div id="content" class="content" role="main">
			<?php if(is_front_page()): get_template_part( 'home', 'news' ); get_template_part('home', 'pined'); endif;?>
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<?php do_action('presscore_before_loop'); ?>

					<?php the_content(); ?>

					<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'the7mk2' ), 'after' => '</div>' ) ); ?>

					<?php presscore_display_share_buttons_for_post( 'page' ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; ?>

			<?php else : ?>

				<?php get_template_part( 'no-results', 'page' ); ?>

			<?php endif; ?>

			</div><!-- #content -->

			<?php do_action('presscore_after_content'); ?>

		<?php endif; // if content visible ?>

<?php get_footer(); ?>