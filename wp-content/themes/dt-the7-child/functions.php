<?php
/**
 * Your code here.
 *
 */


	function presscore_post_details_link( $post_id = null, $class = array('details', 'more-link'), $link_text = null ) {
		global $post;

		if ( !$post_id && !$post ) {
			return '';
		}elseif ( !$post_id ) {
			$post_id = $post->ID;
		}

		if ( post_password_required( $post_id ) ) {
			return '';
		}

		if ( ! is_array( $class ) ) {
			$class = explode( ' ', $class );
		}

		$output = '';
		$url = get_permalink( $post_id );

		if ( $url ) {
			$output = sprintf(
				'<a href="%1$s" class="home_post_more" rel="nofollow">%3$s</a>',
				$url,
				esc_attr( implode( ' ', $class ) ),
				is_string( $link_text ) ? $link_text : __( 'Czytaj więcej', 'the7mk2' )
			);
		}

		return apply_filters( 'presscore_post_details_link', $output, $post_id, $class );
	}

add_image_size( 'offert-size', 220, 150, true );
add_image_size( 'pined-size', 130, 110, true );

   register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );

function lenght_cut($string){
	$lenght = strlen($string);
	if($lenght > 58){
		$string = mb_substr($string, '0', '55','utf-8');
		$string .= '...';
	}
	return $string ;
}




if ( ! function_exists( 'presscore_get_breadcrumbs' ) ) :

	/**
	 * Returns breadcrumbs html
	 * original script you can find on http://dimox.net
	 * 
	 * @since 1.0.0
	 * 
	 * @return string Breadcrumbs html
	 */
	function presscore_get_breadcrumbs( $args = array() ) {

		$default_args = array(
			'text' => array(
				'home' => __( 'Strona Główna', 'the7mk2'),
				'category' => __( 'Category "%s"', 'the7mk2'),
				'search' => __( 'Results for "%s"', 'the7mk2'),
				'tag' => __( 'Entries tagged with "%s"', 'the7mk2'),
				'author' => __( 'Article author %s', 'the7mk2'),
				'404' => __( 'Error 404', 'the7mk2'),
			),
			'showCurrent' => 1,
			'showOnHome' => 1,
			'delimiter' => '',
			'before' => '<li class="current">',
			'after' => '</li>',
			'linkBefore' => '<li typeof="v:Breadcrumb">',
			'linkAfter' => '</li>',
			'linkAttr' => ' rel="v:url" property="v:title"',
			'beforeBreadcrumbs' => '',
			'afterBreadcrumbs' => '',
			'listAttr' => ' class="breadcrumbs text-small"'
		);

		$args = wp_parse_args( $args, $default_args );

		$breadcrumbs_html = apply_filters( 'presscore_get_breadcrumbs-html', '', $args );
		if ( $breadcrumbs_html ) {
			return $breadcrumbs_html;
		}

		extract( array_intersect_key( $args, $default_args ) );

		$link = $linkBefore . '<a' . $linkAttr . ' href="%1$s" title="">%2$s</a>' . $linkAfter;

		$breadcrumbs_html .= '<div class="assistive-text">' . __( 'You are here:', 'the7mk2' ) . '</div>';

		$homeLink = home_url() . '/';
		global $post;

		if (is_home() || is_front_page()) {

			if ($showOnHome == 1) {
				$breadcrumbs_html .= '<ol' . $listAttr . '><a href="' . $homeLink . '">' . $text['home'] . '</a></ol>';
			}

		} else {

			$breadcrumbs_html .= '<ol' . $listAttr . ' xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, $homeLink, $text['home']) . $delimiter;

			if ( is_category() ) {

				$thisCat = get_category(get_query_var('cat'), false);

				if ($thisCat->parent != 0) {

					$cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
					$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
					$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);

					if(preg_match( '/title="/', $cats ) ===0) {
						$cats = preg_replace('/title=""/', 'title=""', $cats);
					}

					$breadcrumbs_html .= $cats;
				}

				$breadcrumbs_html .= $before . sprintf($text['category'], single_cat_title('', false)) . $after;

			} elseif ( is_search() ) {

				$breadcrumbs_html .= $before . sprintf($text['search'], get_search_query()) . $after;

			} elseif ( is_day() ) {

				$breadcrumbs_html .= sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
				$breadcrumbs_html .= sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
				$breadcrumbs_html .= $before . get_the_time('d') . $after;

			} elseif ( is_month() ) {

				$breadcrumbs_html .= sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
				$breadcrumbs_html .= $before . get_the_time('F') . $after;

			} elseif ( is_year() ) {

				$breadcrumbs_html .= $before . get_the_time('Y') . $after;

			} elseif ( is_single() && !is_attachment() ) {

				$post_type = get_post_type();
				if ( $post_type !== 'post' ) {

					$post_type_obj = get_post_type_object( $post_type );
					$breadcrumbs_html .= sprintf($link, get_post_type_archive_link( $post_type ), $post_type_obj->labels->singular_name);

					if ($showCurrent == 1) {
						$breadcrumbs_html .= $delimiter . $before . wp_trim_words( get_the_title(), 5 ) . $after;
					}

				} else {

					$cat = get_the_category();
					if ( $cat ) {
						$cat = $cat[0];
						$cats = get_category_parents($cat, TRUE, $delimiter);

						if ( ! is_wp_error( $cats ) ) {
							if ($showCurrent == 0) {
								$cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
							}

							$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
							$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);

							$breadcrumbs_html .= $cats;
						}
					}

					if ($showCurrent == 1) {
						$breadcrumbs_html .= $before . wp_trim_words( get_the_title(), 5 ) . $after;
					}

				}

			} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {

				$post_type_obj = get_post_type_object(get_post_type());
				if ( $post_type_obj ) {
					$breadcrumbs_html .= $before . $post_type_obj->labels->singular_name . $after;
				}

			} elseif ( is_attachment() ) {

				if ($showCurrent == 1) {
					$breadcrumbs_html .= $delimiter . $before . wp_trim_words( get_the_title(), 5 ) . $after;
				}

			} elseif ( is_page() && !$post->post_parent ) {

				if ($showCurrent == 1) {
					$breadcrumbs_html .= $before . wp_trim_words( get_the_title(), 5 ) . $after;
				}

			} elseif ( is_page() && $post->post_parent ) {

				$parent_id  = $post->post_parent;
				$breadcrumbs = array();

				while ($parent_id) {
					$page = get_post($parent_id);
					$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
					$parent_id  = $page->post_parent;
				}

				$breadcrumbs = array_reverse($breadcrumbs);

				for ($i = 0; $i < count($breadcrumbs); $i++) {

					$breadcrumbs_html .= $breadcrumbs[$i];

					if ($i != count($breadcrumbs)-1) {
						$breadcrumbs_html .= $delimiter;
					}
				}

				if ($showCurrent == 1) {
					$breadcrumbs_html .= $delimiter . $before . wp_trim_words( get_the_title(), 5 ) . $after;
				}

			} elseif ( is_tag() ) {

				$breadcrumbs_html .= $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

			} elseif ( is_author() ) {

				global $author;
				$userdata = get_userdata($author);
				$breadcrumbs_html .= $before . sprintf($text['author'], $userdata->display_name) . $after;

			} elseif ( is_404() ) {

				$breadcrumbs_html .= $before . $text['404'] . $after;
			}

			if ( get_query_var('paged') ) {

				$breadcrumbs_html .= $before;

				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
					$breadcrumbs_html .= ' (';
				}

				$breadcrumbs_html .= __( 'Page', 'the7mk2' ) . ' ' . get_query_var('paged');

				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
					$breadcrumbs_html .= ')';
				}

				$breadcrumbs_html .= $after;

			}

			$breadcrumbs_html .= '</ol>';
		}

		return apply_filters( 'presscore_get_breadcrumbs', $beforeBreadcrumbs . $breadcrumbs_html . $afterBreadcrumbs, $args );
	} // end presscore_get_breadcrumbs()

endif;



