
<?php 
	$args = array(
		'post_type'              => 'post',
		'post_status'            => array( 'Publish' ),
		'posts_per_page'         => '2',
		'category__not_in'		 => '43',
	);
	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts()):;?>
		<div class="home_post_head">
			<h2>Aktualności</h2>
			<a href="<?php home_url();?>/aktualnosci"><p>Zobacz wszystkie aktualności</p></a>
		</div>
		<div style="clear:both"></div>
		<?php	
		while ( $the_query->have_posts() ):
			$the_query->the_post();?>
				<article class="post project-odd post-191 type-post status-publish format-standard has-post-thumbnail hentry category-bez-kategorii category-lifestyle-hobby category-marketing tag-business tag-corporate tag-news tag-post tag-tags tag-wordpress category-1 category-4 category-5 description-off home_post">
					<div class="blog-content wf-td" style="width: 70%;">
						<p class="home_post_data"><?php echo get_the_date() ;?></p>
						<h3 class="home_post_title"><a href="<?php echo the_permalink();?>" title="" rel="bookmark"><?php echo get_the_title() ;?></a></h3>
						<span class="home_post_content"><?php the_excerpt() ;?></span>
						<a href="<?php echo the_permalink();?>" class="home_post_more" rel="nofollow">Czytaj dalej</a>
					</div>
				</article>
			<?php
		endwhile;
	endif;?>

