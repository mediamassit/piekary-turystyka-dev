
<?php 
	$select_offert = get_field('select_offert',option);
if($select_offert):
	$args = array(
		'post_type'              => 'offer',
		'post__in'				=>  $select_offert,
		'post_status'            =>  array( 'Publish' ),
		'orderby'				=> 'post__in'
	);
	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts()):;?>
		<div class="home_post_head home_post_head_pined">
			<h2 style="color:#ffc826;">Promowane wycieczki</h2>
			<a href="<?php home_url();?>/oferta-wycieczek"><p>Zobacz ofertę wycieczek</p></a>
		</div>
		<div style="clear:both"></div>
		<?php	
		while ( $the_query->have_posts() ):
			$the_query->the_post();?>
				<a href="<?php the_permalink();?>" class="home-pined-link">
					<article class="post project-odd post-191 type-post status-publish format-standard has-post-thumbnail hentry category-bez-kategorii category-lifestyle-hobby category-marketing tag-business tag-corporate tag-news tag-post tag-tags tag-wordpress category-1 category-4 category-5 description-off home_pined" >
						<div class="blog-content pr-40 home-content-pined"  >
							<?php $alt = get_post_meta(get_post_thumbnail_id($post_id), '_wp_attachment_image_alt', true) ;?>
							<?php $title = get_the_title(get_post_thumbnail_id($post_id)); ?>
							<?php $thumb = get_the_post_thumbnail_url(get_the_id(),'pined-size');?>
							<?php if ($thumb): ;?>
							<img src="<?php echo $thumb;?>" alt="<?php if($alt): echo $alt ; else : echo $title; endif; ?>">
							<?php else: ;?>
							<img src="<?php echo get_stylesheet_directory_uri();?>/img/else2.jpg" alt="">
							<?php endif ;?>
						</div>
						<div class="blog-content home-content-pined"  style="width:70%;">
							<h3 class="home_post_title home-pined-title"><?php echo get_the_title() ;?></h3>
							<?php $term = get_field('term'); 
								$term = lenght_cut($term);
								$place = get_field('from');
								$place = lenght_cut($place);
								$price = get_field('price');
							?>
							<div class="offer-before-info">
								<p class="offer-info no-mb"><strong >Termin:</strong></p>
								<p class="offer-info no-mb"><strong >Wyjazd z:</strong></p>
								<p class="offer-info no-mb"><strong >Cena:</strong></p>
							</div>
							<p class="offer-info no-mb"><strong class="mobile_display">Termin: </strong><?php echo $term;?></p>
							<p class="offer-info no-mb"><strong class="mobile_display">Wyjazd z: </strong><?php echo $place;?></p>
							<p class="offer-info no-mb"><strong class="mobile_display">Cena: </strong><?php echo $price;?></p>
						</div>
					</article>
				</a>
			<?php
		endwhile;
	endif;
endif; ?>

