<?php
/**
 * The Template for displaying all single posts.
 * @package The7
 * @since   1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header( 'single' ); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'header-main' ); ?>

	<?php if ( presscore_is_content_visible() ): ?>

		<?php do_action( 'presscore_before_loop' ); ?>
		<?php $term = get_field('term'); 
			$place = get_field('from');
			$price = get_field('price');
			$title = get_field('title');
		?>
		<div id="content" class="content" role="main">
		<h2 class="offer_title_single"><?php echo $title ;?></h2>
			<div class="pb-30">
				<div class="offer-before-info">
					<p class="offer-info"><strong>Termin:</strong></p>
					<p class="offer-info"><strong>Wyjazd z:</strong></p>
					<p class="offer-info"><strong>Cena:</strong></p>
				</div>
				<p class="offer-info"><strong class="mobile_display">Termin: </strong><?php echo $term;?></p>
				<p class="offer-info"><strong class="mobile_display">Wyjazd z: </strong><?php echo $place;?></p>
				<p class="offer-info"><strong class="mobile_display">Cena: </strong><?php echo $price;?></p>
			</div>
			<?php if ( post_password_required() ): ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php
					do_action( 'presscore_before_post_content' );

					the_content();

					do_action( 'presscore_after_post_content' );
					?>

				</article>

			<?php else: ?>

				<?php get_template_part( 'content-single', str_replace( 'dt_', '', get_post_type() ) ); ?>

			<?php endif; ?>

			<?php /* comments_template( '', true );*/ ?>

		</div><!-- #content -->

		<?php do_action( 'presscore_after_content' ); ?>

	<?php endif; // content is visible ?>

<?php endwhile; endif; // end of the loop. ?>

<?php get_footer(); ?>