<?php
/*
  Plugin Name: tu-plugin
  Description: Zapewnia strukturę oraz funkcjonalności serwisu.
  Author: mediamass.pl
  Author URI: http://mediamass.pl
  Text Domain: tu-plugin
  Domain Path: /languages/
  Version: 1.0.0
*/
require 'structure/index.php';

function tu_load_custom_wp_admin_style() {
	wp_register_style('tu-plugin', plugin_dir_url( __FILE__ ) . 'assets/style.css', false, '1.0.0');
	wp_enqueue_style('tu-plugin');
}
add_action('admin_enqueue_scripts', 'tu_load_custom_wp_admin_style', 500, 2);