<?php
/*** Post types ***/
require 'post-type/offert.php';

/*** Taxonomies ***/
require 'taxonomy/offert_cat.php';

/*** Fields ***/
require 'custom-field/offert.php';
//require 'custom-field/forms.php';
//require 'custom-field/contact.php';
//require 'custom-field/default.php';

require 'settings/website.php';