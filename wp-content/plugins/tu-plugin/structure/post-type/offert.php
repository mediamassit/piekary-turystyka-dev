<?php

function mm_posttype_offer() {
	$labels = array(
		'name'               => _x( 'Oferta', '', 'mm-plugin' ),
		'singular_name'      => _x( 'Oferta', '', 'mm-plugin' ),
		'menu_name'          => _x( 'Oferty wycieczek', '', 'mm-plugin' ),
		'name_admin_bar'     => _x( 'Oferty', '', 'mm-plugin' ),
		'add_new'            => _x( 'Dodaj Ofertę', '', 'mm-plugin' ),
		'add_new_item'       => __( 'Dodaj nową ofertę', 'mm-plugin' ),
		'new_item'           => __( 'Nowa Ooferta', 'mm-plugin' ),
		'edit_item'          => __( 'Edytuj ofertę', 'mm-plugin' ),
		'view_item'          => __( 'Zobacz ofertę', 'mm-plugin' ),
		'all_items'          => __( 'Wszystkie oferty', 'mm-plugin' ),
		'search_items'       => __( 'Szukaj oferty', 'mm-plugin' ),
		'parent_item_colon'  => __( 'Rodzic oferty:', 'mm-plugin' ),
		'not_found'          => __( 'Nie znaleziono ofert.', 'mm-plugin' ),
		'not_found_in_trash' => __( 'Nie znaleziono ofert w koszu.', 'mm-plugin' )
	);
	$rewrite = array(
		'slug'                => 'oferta',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Opis.', 'mm-plugin' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => $rewrite,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_position'       => 7,
		'menu_icon'  		 => 'dashicons-list-view',
		'supports'           => array( 'title', 'editor', 'thumbnail')
	);
	register_post_type( 'offer', apply_filters('mm-theme/plugin/post-type/offer', $args) );
}
add_action( 'init', 'mm_posttype_offer', 0 );